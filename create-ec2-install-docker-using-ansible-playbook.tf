provider "aws" {
  region = "ap-south-1"
}

resource "aws_default_security_group" "mysg" {
  vpc_id      = "vpc-90a548fb"                 # Provide you VPC ID 

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    description = "SSH from VPC"
    from_port   =  80
    to_port     =  80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {

    Name = "Docker"
}
}
resource "aws_instance" "example" {
  ami           = "ami-0e8710d48cc4ea8dd"
  instance_type = "t2.micro"
  key_name      = "docker"
  security_groups = ["${aws_default_security_group.mysg.name}"]

  connection {
    type     = "ssh"
    user     = "ubuntu"
    private_key = file("docker.pem")
    host     = aws_instance.example.public_ip
  }
 provisioner "remote-exec" {
    inline = [
          "sudo apt update",
          "sudo apt install python -y",
          "sudo apt install ansible -y",
          "sudo apt install git -y",
         # "git clone https://gitlab.com/rplnarendra/terraform-install-ansible-run-playbook-docker.git /tmp/ansible_ws",
          "git clone  https://gitlab.com/rplnarendra/Ansible.git  /tmp/ansible_ws",
          "ansible-playbook /tmp/ansible_ws/buildsetup/buildsetup.yml"
    ]
  }
  

}
